# Stamp Sale Want list Processor

Collects a file of stamp names on sale and compares with a file of stamp names in a want list.
Javascript gets the stamps on sale - python has the want list and does the processing.

    Use the Javascript 'index_static.js' with axios and cheerio 
    to scrape stamps on sale and produce a .csv file. 
     
    Stamp Sale Processor opens a .csv stamp want list and parses it 
    into a list of strings like '{Country} {Scott#}'.

    The stamps on sale .csv from the Javascript is opened and parsed similarly.
    
    Finally the intersection of the want list and the stamps on sale is returned.
    The user can then look into these. 
