'''
    Use the Javascript 'index_static.js' with axios and cheerio 
    to scrape stamps on sale and produce a .csv file. 
     
    Stamp Sale Processor opens a .csv stamp want list and parses it 
    into a list of strings like '{Country} {Scott#}'.

    The stamps on sale .csv from the Javascript is opened and parsed similarly.
    
    Finally the intersection of the want list and the stamps on sale is returned.
    The user can then look into these. 
'''
import csv
import re
import fileinput
import string

rough_want_list = 'rough_form_want_list.csv'
stamps_on_sale_csv_file = '1609783862581stamps_on_sale.csv'


def main():
    # parse want list
    want_list = open_the_rough_want_list(rough_want_list)
    need_these_rows = clean_the_want_list_file(want_list)
    remove_gots = remove_stamps_not_wanted_from_the_want_list(need_these_rows)
    no_empty_rows = remove_empty_rows_from_the_want_list(remove_gots)
    two_column_rows = reduce_the_want_list_to_two_columns(no_empty_rows)
    what_i_want = merge_the_two_columns_to_make_a_stamp_name(two_column_rows)
    print(what_i_want)
    
    # parse on sale list
    stamps_on_sale_list = open_the_stamps_on_sale_list(stamps_on_sale_csv_file)
    bad_words_removed = remove_unwanted_words(stamps_on_sale_list)
    years_removed = remove_years_from_stamps_on_sale_file(bad_words_removed)
    removed_double_spaces = remove_double_and_longer_spaces(years_removed)
    what_is_for_sale = remove_space_at_end_of_each_element(removed_double_spaces)

    print(intersection(what_i_want, what_is_for_sale)) 
    
''' 
    Methods for combining the data.
'''
def intersection(lst1, lst2): 
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3 
    
''' 
    Methods for parsing the want list data.
'''
def open_the_rough_want_list(data):
    with open(data, newline='') as csvfile:
        data = list(csv.reader(csvfile))
    return data

def clean_the_want_list_file(data):
    need_these_rows = []
    pattern = re.compile(
        'Complete|Totals|LIMAYE|SCOTT|Collection|Sheet|Mint|Country|Summary|Countries|COUNTRY') 
    for line in data:
        need_these_rows.append([s for s in line if pattern.search(s) == None])
    return need_these_rows

def remove_stamps_not_wanted_from_the_want_list(data):
    remove_gots = []
    for row in data:
        if (row[4] == '' ):
            remove_gots.append(row)
    return remove_gots

def remove_empty_rows_from_the_want_list(data):
    no_empty_rows = []
    for row in data:
        if [x for x in row if x]: # non empty list
            no_empty_rows.append(row)
    return no_empty_rows

def reduce_the_want_list_to_two_columns(data):
    two_column_rows = []
    for row in data:
        two_column_rows.append(row[1:3] )
    return two_column_rows
        
def listToString(s):  
    str1 = " " 
    return (str1.join(s))  
     
def merge_the_two_columns_to_make_a_stamp_name(data): ### {Country, Scott#} ###        
    merged_column_rows = []
    for row in data:
        merged_column_rows.append(listToString(row) )
    return merged_column_rows

''' 
    Methods for parsing the stamps on sale data.
'''  

def open_the_stamps_on_sale_list(data):
    with open(data, newline='') as csvfile:
        data = list(csv.reader(csvfile))
    return data[0]

def remove_unwanted_words(data):
    bad_word_list = ['Scott', 'SCOTT', 'Used', 'â€”', 'REGULAR ISSUES', 'OFFICIAL','ISSUES','USED','SCV','- U -','*','#','%',' - ',
                    'mint','reprint','CV','A:','AA:','AH:','P:','most','MNH','C:','A ','AV:', 'used','signed','faults',
                    '(',')', 'perf 11','un no gum','with','cert','F:','spacefiller', 'small perf']
    bad_words_removed = []
    for line in data:
        for word in bad_word_list:
            if word in line:
                line = line.replace(word,'')
        bad_words_removed.append(line)
    return bad_words_removed

def remove_double_and_longer_spaces(data):
    removed_double_spaces = []
    for line in data:
        line = re.sub('\s+',' ', line)
        removed_double_spaces.append(line)
    return removed_double_spaces
 
def remove_years_from_stamps_on_sale_file(data):
    years_removed = []
    for line in data:
        line = re.sub(" \d{4} ", " ", line)
        line = re.sub(" \$\d*", " ", line)
        line = re.sub(" \.\d*", " ", line)
        years_removed.append(line)
    return years_removed 

def remove_space_at_end_of_each_element(data):
    removed_space = []
    for line in data:
        line = line.strip()
        removed_space.append(line)
    return removed_space

def Convert(string): 
    li = list(string.split(",")) 
    return li 

if __name__ == "__main__":
    main() 