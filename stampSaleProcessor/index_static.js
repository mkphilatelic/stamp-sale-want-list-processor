
/**
 * This is a script for axios and cheerio that will 
 * scrape the hipstamp by iterating over a given rest api call.
 * 
 * Returns a .csv file of stamps on sale.
 * 
 * To be opened by the Stamp Sale Processor.
 * 
 * Run:
 * 
 * node index_static.js >> new_stamps_on_sale.csv
 * 
 */

const axios = require("axios");
const cheerio = require("cheerio");
const { connected } = require("process");

categories = ['africa/1', 'united-states/12','asia/2', 'australia-oceania/3', 'british-commonwealth/4', 'canada/5', 'caribbean/6','europe/7','great-britain/8', 'central-south-america/9','middle-east/10'];
file_name = Date.now()+"stamps_on_sale.csv"
var fs = require('fs');

for (category in categories) {
  axios
   //.get('https://www.hipstamp.com/category/'+categories[category]+'/?item_specifics_07_year_of_issue[from]=1840&item_specifics_07_year_of_issue[to]=1940&show_only[]=on_sale50&item_specifics_04_condition=used&item_specifics_05_stamp_format=single&country=1903&sort=catalog&limit=96')
    .get('https://www.hipstamp.com/store/evergreene-stamps/?utm_source=Seller%20Newsletter&utm_medium=email&utm_campaign=greenestamps-20210104-28028')
    .then((response) => {

      const html = response.data;
      var str, $ = cheerio.load(html, {xmlMode: true});     
      str = $('script:not([src])')[0].children[0].data;
      var res = str.replace(/= /g ,'!').split("!")[3]
      json_list = res.substring(0, res.length-2);
      var JsonObject= JSON.parse(json_list);

      stamps_on_sale = []
      
      for(var item in JsonObject[0]["ecommerce"]["impressions"]) {
        stamps_on_sale.push(JsonObject[0]["ecommerce"]["impressions"][item]["name"])
      }

      for(var item in JsonObject[1]["ecommerce"]["impressions"]) {
        stamps_on_sale.push(JsonObject[1]["ecommerce"]["impressions"][item]["name"])
      }
      console.log(stamps_on_sale)
      fs.appendFile(file_name, stamps_on_sale, (err) => {
        if (err) throw err;
        console.log('The "data to append" was appended to file '+file_name);
      });
    }    
  )   
} 

console.log(file_name)