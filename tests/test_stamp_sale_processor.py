import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

from stampSaleProcessor.stamp_sale_processor import *

def test_clean_the_want_list_file():
    want_list = ['Austria 11 Complete', 'Canada Scott 43', 'Great Britain Mint Summary COUNTRY']
    cleaned = clean_the_want_list_file(want_list)
    print(cleaned)  
    assert ['A', 'u', 's', 't', 'r', 'i', 'a', ' ', '1', '1', ' ', 'C', 'o', 'm', 'p', 'l', 'e', 't', 'e'] in cleaned

def test_remove_stamps_not_wanted_from_the_want_list():
    need_these_rows_to_go = [['', 'Removed', '227', '', '1', '', '', '', '', '  -   ', '', '', '  -   ', '']]
    need_these_rows_to_stay = [['', 'Stays', '227', '', '', '', '', '', '', '  -   ', '', '', '  -   ', '']]

    assert remove_stamps_not_wanted_from_the_want_list(need_these_rows_to_go) == [] 
    assert remove_stamps_not_wanted_from_the_want_list(need_these_rows_to_stay)  == [['', 'Stays', '227', '', '', '', '', '', '', '  -   ', '', '', '  -   ', '']]

def test_remove_double_and_longer_spaces():
    doublespaces_to_remove =  ['There    should be no     double or lo    ng  er spaces']
    cleaned = remove_double_and_longer_spaces(doublespaces_to_remove)
    assert cleaned == ['There should be no double or lo ng er spaces']
 
def test_remove_space_at_end_of_each_element():
    space_at_end_to_remove =  ['There should be no space at the end ,','not here too ,']
    cleaned = remove_space_at_end_of_each_element(space_at_end_to_remove)
    assert cleaned == ['There should be no space at the end,', 'not here too,']

def test_remove_years_from_stamps_on_sale_file():
    years_to_remove =  ['Belgium 1956 154']
    cleaned = remove_years_from_stamps_on_sale_file(years_to_remove)
    assert cleaned == ['Belgium 154']